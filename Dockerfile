FROM registry.fedoraproject.org/fedora:33

RUN mkdir -p /logs
VOLUME /logs

RUN mkdir -p /data

CMD ["/bin/bash"]
